import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { AuthRestService } from '../shared/auth-rest.sevice';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private router: Router,
    private authRestService: AuthRestService,
  ) { }

  ngOnInit() {
    this.form = new FormGroup( {
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    console.log(this.form);
    if(!this.form.valid) {
      return;
    }
    this.authRestService.authUser(this.form.get('login').value, this.form.get('password').value)
    .subscribe(res => {
      if(res['status'] == 'success') {
        console.log(res['token'])
        localStorage.setItem( this.form.value['login'], res['token']);
        this.router.navigate(['/']);
      } else {
        console.log('error')
        this.router.navigate(['/register']);
      }
    });
    //localStorage.setItem('dar-lab-auth', this.form.value['login']);
    
    

  }

}
