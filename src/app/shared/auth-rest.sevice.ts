import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import {Student} from '../students/student.types'
@Injectable({
    providedIn: 'root'
})
export class AuthRestService {

    host = 'http://localhost:3000/users';
  
    constructor(private http: HttpClient) {
  
    }

    authUser(username: string, password: string) {
        return this.http.post(`${this.host}/auth`, {username,password});
    }

    registrateUser(username: string, password: string) {
        return this.http.post(this.host, {username,password});
    }
}
