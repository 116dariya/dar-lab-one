import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthRestService } from '../shared/auth-rest.sevice';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  formSubmitted = false;

  constructor(
    private router: Router,
    private authRestService: AuthRestService,
  ) { }

  ngOnInit() {
    this.form = new FormGroup( {
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required)
    });
  }

  registration() {
    this.formSubmitted = true;

    console.log(this.form);
    if(!this.form.valid) {
      return;
    }
    
    //if(this.form.get('password') === this.form.get('confirmPassword')) {
      console.log("mathc")
      this.authRestService.registrateUser(this.form.get('login').value, this.form.get('password').value)
      .subscribe(res => {
        console.log(res)
      })
    // } else {
    //   console.warn("Passwords do not match");
    //   return;
    // }
    

    // if(this.form.get('password') !== this.form.get('confirmPassword')) {
    //   console.warn('Passwords do not match')
    //   return;
    // }
    localStorage.setItem('dar-lab-auth', this.form.value['login']);
    this.router.navigate(['/login']);
  }

}
