import { Component, OnInit } from '@angular/core';
import {Faculty} from '../faculty.types'
import { Router } from '@angular/router';
import { FacultyRestService } from 'src/app/shared/faculties-rest.service';

@Component({
  selector: 'app-faculty-list',
  templateUrl: './faculty-list.component.html',
  styleUrls: ['./faculty-list.component.scss']
})
export class FacultyListComponent implements OnInit {

  
  searchQuery = '';

  faculties: Faculty[];

  facultiesToShow: Faculty[] = [];
  constructor(
    private facultyRestService: FacultyRestService,
    private router: Router,
  ) { }


  ngOnInit() {
    this.facultyRestService.getFaculties()
    .subscribe(faculties => {
      this.faculties = faculties;
      this.facultiesToShow = [...this.faculties];
    });
  }



  facultyClicked(faculty: Faculty) {
    this.router.navigate(['faculties', 'faculty', faculty.id]);
  }

  search() {
    if (!this.searchQuery) {
      this.facultiesToShow = [...this.faculties];
    }
    this.facultiesToShow = this.facultiesToShow.filter(f => f.name.includes(this.searchQuery));
  }
}
