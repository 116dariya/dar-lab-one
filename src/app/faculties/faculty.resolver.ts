import { Faculty} from './faculty.types';
import {Resolve, ActivatedRouteSnapshot} from '@angular/router';
import { Injectable } from '@angular/core';
import {FacultyRestService} from '../shared/faculties-rest.service';
import { Observable } from 'rxjs';

@Injectable()

export class FacultyResolver implements Resolve<Faculty> {

    constructor(private facultyRestService: FacultyRestService){}

    resolve(route: ActivatedRouteSnapshot): Observable<Faculty> {
        return this.facultyRestService.getFaculty(route.paramMap.get('id'));
    }

}